import { useEffect, useState } from 'react';
import './App.css';
import { ethers } from 'ethers'
import { useForm } from "react-hook-form";
import clsx from "clsx";

import Logo from "./i/3.jpg";

import ABI from './abi.json'
const scAddress = "0x797194515f84b45ca597716c500ea28f6ff84b80";
const maxSupply = 5555;

function App() {
  const [mintsAvailable, setMintsAvailable] = useState(0);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
    watch,
  } = useForm({ defaultValues: { amount: 1, } });

  const amount = watch("amount");

  const onSubmit = async (data) => {
    await mint();
  };

  useEffect(() => {
    async function fetchData() {
      let isWalletInstalled = await checkEthereumNetwork();
      if (!isWalletInstalled) return;

      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(scAddress, ABI, provider)
      try {
        const totalSupply = await contract.totalSupply();
        setMintsAvailable(maxSupply - totalSupply.toNumber());
      } catch (err) {
        console.log("Error: ", err)
      }
    };

    fetchData();
  }, []);

  const mint = async () => {
    let isWalletInstalled = await checkIfWalletInstalled();
    if (!isWalletInstalled) return;

    if (amount > 500) {
      alert("Can only mint 500 NFTs at once.")
      return;
    }

    try {
      const overrides = {
        value: ethers.utils.parseEther("" + (0.00001 * amount)),
      }

      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const contract = new ethers.Contract(scAddress, ABI, signer);
      const transaction = await contract.mint(amount, overrides);
      let result = await transaction.wait();

      if (result && result.blockNumber > 0) {
        console.log(result);
        alert('Congratulations! You\'ve successfully minted your NFT.');
      }
    } catch (err) {
      processSCError(err);
    }
  }

  const processSCError = (err) => {
    err = err + '';
    console.log("err", err);

    // MetaMask Tx Signature: User denied transaction signature.
    if (err && err.code === 4001) return;
    if (err.includes('User denied transaction')) return;
    if (err.includes('user rejected transaction')) return;

    let message = "Unexpected error. Please contact support.";
    if (err && err.data && err.data.message) message = err.data.message;
    if (err && err.data && err.data.message) message = err.data.message;

    let errorParts = err.split('","data":{"originalError');
    if (errorParts.length > 1) {
      let errorParts2 = errorParts[0].split('"message":"');
      if (errorParts2.length > 0) message = errorParts2[1];
    }

    if (err.includes('"code":-32000')) message = "You do not have enough ETH";

    message = message.replace('Error: VM Exception while processing transaction: reverted with reason string', '');
    message = message.replace(/'/g, "");
    message = message.replace('execution reverted: ', '');
    alert(message);
  }

  const checkEthereumNetwork = async () => {
    if (!window.ethereum) {
      alert("Please install MetaMask.");
      return false;
    }

    let chainId = await window.ethereum.request({ method: 'eth_chainId' });
    console.log(chainId);

    if (chainId !== "0x1") {
      alert('Please switch network to Ethereum Mainnet');
      return false;
    }

    /*
    if (chainId !== "0x539") {
      alert('Please switch to Ethereum Localhost');
      return false;
    }
    */

    /*
    if (chainId !== "0x4") {
      alert('Please switch to Ethereum Rinkeby');
      return false;
    }
    */

    return true;
  }

  // request access to the user's MetaMask account
  const requestAccount = async () => {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
  }

  const checkIfWalletInstalled = async () => {
    let result = await checkEthereumNetwork();
    if (!result) return;

    await requestAccount();

    if (window.ethereum && !window.ethereum.isConnected()) {
      alert('Your wallet is not connected. Please reload this page.');
      return false;
    }

    let currentAccount = await window.ethereum.request({ method: 'eth_accounts' });

    if (!currentAccount || currentAccount.length === 0 || currentAccount[0].length < 10) {
      alert('Please signin with your Ethereum account in MetaMask.');
      return false;
    }

    return true;
  }

  return (
    <>
      <img src={Logo} alt="" />
      <form onSubmit={handleSubmit(onSubmit)}>

        <input
          type="number"
          className="amountInput"
          placeholder="1"
          {...register("amount", {})}
        />

        <button
          className={clsx(
            "b",
          )}
          type="submt" href="https://opensea.io/collection/gliese-zombies-nft-v2">Mint</button>
      </form>
      <span className="availableToMint">Available: <b>{mintsAvailable}</b> NFTs</span>

      <a href="https://opensea.io/collection/gliese-zombies-nft-v2" target="_blank" rel="noreferrer" className="openSea">View Collection on OpenSea</a>
    </>
  );
}

export default App;
